#include "NPC.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Player.h"
#include "AABB.h"

#include <stdexcept>
#include <string>
#include <stdlib.h>

using std::string;

/**
 * NPC
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
NPC::NPC() : Sprite()
{
    state = IDLE;
    speed = 50.0f;

    maxRange = 0.4f;
    timeToTarget = 0.25f;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    // Could pass these in to change the difficulty
    // of a NPC for each level etc. 
    maxRange = 0.25f;
    timeToTarget = 0.25f;
    health = 100;
    points = 10;
}

/**
 * init
 * 
 * Function to populate an animation structure from given parameters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void NPC::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/SploidSS.png");
    string bossPath("assets/images/SploidBossSS.png");
    int randomChoice = rand() % 3;

    //position
    Vector2f position(300.0f,300.0f);

    if (randomChoice == 2)
    {
        // Sometimes generate a boss instead of a normal enemy
        // Call sprite constructor
        Sprite::init(renderer, bossPath, 6, &position);
        health += 100;
        points += 10;
        speed -= 15.0f;

    }
    else
    {
        // Call sprite constructor
        Sprite::init(renderer, path, 6, &position);
        speed += 10.0f;
    }

    
    

    // Setup the animation structure
    animations[LEFT]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[DEAD]->init(4, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 4);

    animations[DEAD]->setLoop(false);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.4f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}

/**
 * ~NPC
 * 
 * Destroys the NPC and any associated 
 * objects 
 * 
 */
NPC::~NPC()
{

}

void NPC::update(float dt)
{
    if(state != DEAD) //don't move dead sprite
        ai();
    else    
    {
        velocity->setX(0.0f);
        velocity->setY(0.0f);
    }
    
    Sprite::update(dt);
}

/**
 * ai
 * 
 * Adjusts the velocity / state of the NPC
 * to follow the player
 * 
 */
void NPC::ai()
{
    // get player
    Player* player = game->getPlayer();

    // get distance to player
        // copy the player position
    Vector2f vectorToPlayer(player->getPosition());
        // subtract our position to get a vector to the player
    vectorToPlayer.sub(position);
    float distance = vectorToPlayer.length();

    // if player 'in range' stop and fire
    if(distance < maxRange)
    {
        // put fire code in here later!
    }   
    else
    {
        // else - head for player

        // Could do with an assign in vector
        velocity->setX(vectorToPlayer.getX());
        velocity->setY(vectorToPlayer.getY());   
        
        // will work but 'wobbles'
        //velocity->scale(speed);   

        // Arrive pattern 
        velocity->scale(timeToTarget);

        if(velocity->length() > speed)
        {
            velocity->normalise();
            velocity->scale(speed);            
        }

        state = IDLE;

        if(velocity->getY() > 0.1f)
            state = DOWN;
        else
            if(velocity->getY() < -0.1f)
                state = UP;
            // empty else is not an error!
        
        if(velocity->getX() > 0.1f)
            state = RIGHT;
        else
            if(velocity->getX() < -0.1f)
                state = LEFT;
            // empty else is not an error. 
    }
}

void NPC::setGame(Game* game)
{
    this->game = game;
}

int NPC::getCurrentAnimationState()
{
    return state;
}

void NPC::takeDamage(int damage)
{
    health -= damage;

    if(!(health > 0))
        state = DEAD;
}

bool NPC::isDead()
{
    if (health > 0)
        return false;
    else
        // Take out this line if there are issues
        // Seems to fix issue where points can still be gathered after death
        state = DEAD;
        return true;
}

void NPC::respawn(const int MAX_HEIGHT, const int MAX_WIDTH)
{
    health = 100;
    state = IDLE;

    Vector2f randomPosition;

    int doubleWidth = MAX_WIDTH * 2;
    int doubleHeight = MAX_HEIGHT *2;

    // get a random number between 0 and 
    // 2 x screen size. 
    int xCoord = rand()%doubleWidth;
    int yCoord = rand()%doubleHeight;

    // if its on screen move it off. 
    if(xCoord < MAX_WIDTH)
        xCoord *= -1;

    if(yCoord < MAX_HEIGHT)
        yCoord *= -1; 

    randomPosition.setX(xCoord);
    randomPosition.setY(yCoord);

    this->setPosition(&randomPosition);    
}
    
int NPC::getPoints()
{
    return points;
}

void NPC::setPoints(int pointValue)
{
    points = pointValue;
}
    